using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace PS2
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Enemy : Microsoft.Xna.Framework.DrawableGameComponent
    {
        ICelAnimationManager celAnimationManager1;
        IInputHandler inputHandler;
        private Vector2 position;
        private int health;
        float timer;
        int switc;
        public static Random random;
        public Vector2 Position
        {
            get { return position; }
        }
        private readonly float moveRate;
        private string goblinKey;
        private SpriteBatch spriteBatch;
        public bool removed;
        // Acts according to player position and behaviour
        private Player player;

        public Enemy(Game game, Player player, Vector2 pos)
            : base(game)
        {
            celAnimationManager1 = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            position = pos;
            random = new Random();
            removed = false;
            moveRate = 70.0f;
            goblinKey = "enemyWalkDown";
            this.player = player;
            health = 50;
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {

            CelRange celRange1 = new CelRange(1, 1, 9, 1);
            CelRange celRange2 = new CelRange(1, 2, 9, 2);
            CelRange celRange3 = new CelRange(1, 3, 9, 3);
            CelRange celRange4 = new CelRange(1, 4, 9, 4);


            //celAnimationManager.AddAnimation("sonicRun", "goblin", celCount, 10);

            celAnimationManager1.AddAnimation("enemyWalkDown", "enemy", celRange3, 64, 64, 9, 10);
            celAnimationManager1.AddAnimation("enemyWalkRight", "enemy", celRange4, 64, 64, 9, 10);
            celAnimationManager1.AddAnimation("enemyWalkUp", "enemy", celRange1, 64, 64, 9, 10);
            celAnimationManager1.AddAnimation("enemyWalkLeft", "enemy", celRange2, 64, 64, 9, 10);


            //celAnimationManager.ToggleAnimation("goblinWalkRight");
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {

            // If the player is far enough, walk random.
            if (Vector2.Distance(position, player.Position) <= 400)
            {

                if (position.X >= player.Position.X + 30)
                    {
                        position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkLeft";
                    }
                    else if (position.X < player.Position.X - 30)
                    {
                        position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkRight";
                    }
                    else if (position.Y > player.Position.Y + 30)
                    {
                        position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkUp";

                    }
                    else if (position.Y < player.Position.Y - 55)
                    {
                        position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkDown";
                    }

                if (((player.Position.X > Position.X &&
                player.Position.X < Position.X + 60) ||
                 (player.Position.X  < Position.X &&
                 player.Position.X > Position.X - 60)) &&
                 ((player.Position.Y > Position.Y &&
                 player.Position.Y < Position.Y + 60) ||
                 (player.Position.Y < Position.Y &&
                 player.Position.Y > Position.Y -60)))
                {

                    switch (random.Next(4))
                    {
                        case 0:
                            break;
                        case 1:
                            player.health -= 1;
                            break;
                        case 2:
                            player.health -= 2;
                            break;
                        case 3:
                            player.health -= 3;
                            break;
                    }
                }
            }
            else
            {
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(timer > 3)
                {
                    switc = random.Next(4);
                    timer = 0;
                }
                switch (switc)
                {
                    case 0:
                        position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkLeft";
                        break;
                    case 1:
                        position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkRight";
                        break;
                    case 2:
                        position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkUp";
                        break;
                    case 3:
                        position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        goblinKey = "enemyWalkDown";
                        break;
                }
            }
           if (((player.Position.X > Position.X &&
                player.Position.X < Position.X + 60) ||
                 (player.Position.X < Position.X &&
                 player.Position.X > Position.X - 60)) &&
                 ((player.Position.Y > Position.Y &&
                 player.Position.Y < Position.Y + 60) ||
                 (player.Position.Y < Position.Y &&
                 player.Position.Y > Position.Y - 60))&& inputHandler.KeyboardHandler.IsKeyDown(Keys.Space))
           {
                health -= random.Next(18);
                if(health <= 0)
                   removed = true;
           }


            if (position.X > Game.GraphicsDevice.Viewport.Width - 50)
                position.X = Game.GraphicsDevice.Viewport.Width - 50;
            if (position.X < 0)
                position.X = 0;
            if (position.Y > Game.GraphicsDevice.Viewport.Height - 50)
                position.Y = Game.GraphicsDevice.Viewport.Height - 50;
            if (position.Y < 0)
                position.Y = 0;

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            celAnimationManager1.Draw(gameTime, goblinKey, spriteBatch, position, SpriteEffects.None);
            spriteBatch.End();
        }
    }
}
