using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace PS2
{
    public class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;
        private string goblinKey;
        private string direction;
        SpriteBatch spriteBatch;
        public int health;

        private Vector2 position = new Vector2(200, 200);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        private float moveRate = 100.0f;

        public Player(Game game)
            : base(game)
        {
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            goblinKey = "goblinPause";
            direction = "Down";
            health = 500;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        protected override void LoadContent()
        {
            CelRange celRange1 = new CelRange(1, 1, 6, 1);
            CelRange celRange2 = new CelRange(1, 2, 6, 2);
            CelRange celRange3 = new CelRange(1, 3, 6, 3);
            CelRange celRange4 = new CelRange(1, 4, 6, 4);
            CelRange celRange5 = new CelRange(7, 1, 7, 1);
            CelRange celRange6 = new CelRange(7, 2, 7, 2);
            CelRange celRange7 = new CelRange(7, 3, 7, 3);
            CelRange celRange8 = new CelRange(7, 4, 7, 4);
            CelRange celRange9 = new CelRange(8, 1, 11, 1);
            CelRange celRange10 = new CelRange(8, 2, 11, 2);
            CelRange celRange11 = new CelRange(8, 3, 11, 3);
            CelRange celRange12 = new CelRange(8, 4, 11, 4);


            celAnimationManager.AddAnimation("goblinWalkDown", "goblin", celRange1, 64, 64, 6, 10);
            celAnimationManager.AddAnimation("goblinWalkRight", "goblin", celRange2, 64, 64, 6, 10);
            celAnimationManager.AddAnimation("goblinWalkUp", "goblin", celRange3, 64, 64, 6, 10);
            celAnimationManager.AddAnimation("goblinWalkLeft", "goblin", celRange4, 64, 64, 6, 10);
            celAnimationManager.AddAnimation("goblinPauseDown", "goblin", celRange5, 64, 64, 1, 10);
            celAnimationManager.AddAnimation("goblinPauseRight", "goblin", celRange6, 64, 64, 1, 10);
            celAnimationManager.AddAnimation("goblinPauseUp", "goblin", celRange7, 64, 64, 1, 10);
            celAnimationManager.AddAnimation("goblinPauseLeft", "goblin", celRange8, 64, 64, 1, 10);
            celAnimationManager.AddAnimation("goblinKikDown", "goblin", celRange9, 64, 64, 4, 10);
            celAnimationManager.AddAnimation("goblinKikRight", "goblin", celRange10, 64, 64, 4, 10);
            celAnimationManager.AddAnimation("goblinKikUp", "goblin", celRange11, 64, 64, 4, 10);
            celAnimationManager.AddAnimation("goblinKikLeft", "goblin", celRange12, 64, 64, 4, 10);

            //celAnimationManager.ToggleAnimation("goblinWalkRight");
        }

        public override void Update(GameTime gameTime)
        {

            int celWidth = celAnimationManager.GetAnimationFrameWidth("sonicRun");
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                //celAnimationManager.ResumeAnimation("goblinWalkRight");
                goblinKey = "goblinWalkRight";
                direction = "Right";
                position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                //celAnimationManager.ResumeAnimation("goblinWalkLeft");
                goblinKey = "goblinWalkLeft";
                direction = "Left";
                position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Up))
            {
                //celAnimationManager.ResumeAnimation("goblinWalkUp");
                goblinKey = "goblinWalkUp";
                direction = "Up";
                position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Down))
            {
                //celAnimationManager.ResumeAnimation("goblinWalkDown");
                goblinKey = "goblinWalkDown";
                direction = "Down";
                position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Space))
            {
                goblinKey = "goblinKik" + direction;
            }
            else
            {
                //celAnimationManager.PauseAnimation("goblinPause"+direction);
                goblinKey = "goblinPause"+direction;
            }


            if (position.X > Game.GraphicsDevice.Viewport.Width - 60)
                position.X = Game.GraphicsDevice.Viewport.Width - 60;
            if (position.X < -10)
                position.X = -10;
            if (position.Y > Game.GraphicsDevice.Viewport.Height - 60)
                position.Y = Game.GraphicsDevice.Viewport.Height - 60;
            if (position.Y < 0)
                position.Y = 0;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            celAnimationManager.Draw(gameTime, goblinKey, spriteBatch, position, SpriteEffects.None);
            spriteBatch.End();
        }
    }
}
