using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


using XELibrary;

namespace PS2
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private int enemy_killed;
        private CelAnimationManager celAnimationManager;
        ScrollingBackgroundManager scrollingBackgroundManager;
        private InputHandler inputHandler;
        Texture2D healthTex;
        Rectangle healthRec;
        Song song;
        private SpriteFont timeFont;
        private Vector2 pos;
        private Vector2 announceFontPos;
        private Vector2 numFontPos;
        private int viewportWidth;
        private int viewportHeight;
        public static Random random;
        private Player player;
        Texture2D gameOver;
        Texture2D gameWon;
        float timer;
        public List<Enemy> enemys;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 600;
           
            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManager = new CelAnimationManager(this, "Textures\\");
            Components.Add(celAnimationManager);

            scrollingBackgroundManager = new ScrollingBackgroundManager(this, "Textures\\");
            Components.Add(scrollingBackgroundManager);

            player = new Player(this);
            Components.Add(player);
            enemy_killed = 0;
        }

        protected override void Initialize()
        {
            viewportHeight = graphics.GraphicsDevice.Viewport.Height;
            viewportWidth = graphics.GraphicsDevice.Viewport.Width;

            numFontPos = new Vector2(viewportWidth - 100, 0);
            announceFontPos = new Vector2(viewportWidth / 2 - 200, viewportHeight / 2 - 150);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            healthTex = Content.Load<Texture2D>(@"Textures\health");
            song = Content.Load<Song>("song");
            MediaPlayer.Play(song);
            scrollingBackgroundManager.AddBackground("back", "grass", new Vector2(0, 0), new Rectangle(0, 0, 1280, 720), 10, 0.5f, Color.White);
            gameOver = Content.Load<Texture2D>(@"Textures\game_over");
            gameWon = Content.Load<Texture2D>(@"Textures\youwon");
            player.Load(spriteBatch);
            timeFont = Content.Load<SpriteFont>(@"Fonts\Arial");
            enemys = new List<Enemy>();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right) && player.Position.X >= (graphics.GraphicsDevice.Viewport.Width / 2.0f))
                scrollingBackgroundManager.ScrollRate = -4.0f;
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left) && player.Position.X <= 100f)
                scrollingBackgroundManager.ScrollRate = 4.0f;
            else
                scrollingBackgroundManager.ScrollRate = 0.0f;
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (timer > 3)
            {
                timer = 0;
                random = new Random();
                pos = new Vector2(1700, random.Next(600));
                enemys.Add(new Enemy(this, player, pos));
                Components.Add(enemys[enemys.Count - 1]);
                enemys[enemys.Count - 1].Load(spriteBatch);
            }
           
            for (int i=0; i < enemys.Count; i++)
            {
                if (enemys[i].removed)
                {
                    Components.Remove(enemys[i]);
                    enemys.RemoveAt(i);
                    enemy_killed++;
                    i--;
                }
            }
            if (player.health <= 0 || enemy_killed >= 20)
                Components.Remove(player);
            healthRec = new Rectangle(50, 20, player.health, 20);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.BackToFront);
            spriteBatch.DrawString(timeFont, enemy_killed.ToString(), numFontPos, Color.White);
            if (enemy_killed >= 20)
                spriteBatch.Draw(gameWon, announceFontPos, Color.White);
            else if (player.health <= 0)
                spriteBatch.Draw(gameOver, announceFontPos , Color.White);
            scrollingBackgroundManager.Draw("back", spriteBatch);
            spriteBatch.Draw(healthTex, healthRec, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
